import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Typed from 'typed.js';
declare var createCards: any;
declare var BancorConvertWidget: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, AfterViewInit {
  private mediumFeed = ' https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fmedium.com%2Ffeed%2F%40canyacoin';
  components = [
    {
      logo: 'CanApps.svg',
      description: 'CanApps are decentralised applications built using web3 technologies that target key niches and verticals in the global peer-to-peer services industry.',
      url: 'https://canya.com'
    },
    {
      logo: 'CanYaCore.svg',
      description: 'CanYaCore is infrastructure to building the ecosystem - a stack of open-source frameworks, templates and contracts such as the escrow, identity management and support tasking components.',
      url: 'https://github.com/canyaio/'
    },
    {
      logo: 'CanYaDAO.svg',
      description: 'CanYaDAO forms the scalable and decentralised support, maintenance and governance backend to each CanApp.',
      url: 'https://canyadao.com'
    }
  ];
  canApps = [
    {
      name: 'CanInvoice',
      desc: 'Easily create your own clean, professional and accurate invoices',
      url: 'https://caninvoice.io',
      type: 'helper'
    },
    {
      name: 'CanSend',
      desc: 'Send ERC20 tokens to multiple addresses at once',
      url: 'https://CanSend.io',
      type: 'helper'
    },
    {
      name: 'CanStation',
      desc: 'Find out the ideal amount of gas to allocate to your ETH transaction',
      url: 'https://CanStation.io',
      type: 'helper'
    },
    {
      name: 'CanSeek',
      desc: 'Hire and be hired with decentralised technology',
      url: 'https://CanSeek.io',
      type: 'main'
    },
    {
      name: 'CanWork',
      desc: 'The decentralised services marketplace',
      url: 'https://Canya.com',
      type: 'main'
    },
    {
      name: 'CanShare',
      desc: 'Send and receive files using distributed technology',
      url: 'https://canshare.io/',
      type: 'helper'
    },
    {
      name: 'CanTrack',
      desc: 'A crisp and easy to use task tracker to help you with your project',
      url: 'https://CanTrack.io',
      type: 'helper'
    },
    {
      name: 'CanSign',
      desc: 'Upload documents to IPFS and sign them with your Ethereum address.',
      url: 'https://CanSign.io',
      type: 'helper'
    }
  ];
  exchanges = [
    {
      name: 'KuCoin',
      url: 'https://www.kucoin.com/#/trade.pro/CAN-BTC'
    },
    {
      name: 'xBrick',
      url: 'https://xbrick.io/',
    },
    {
      name: 'AEX',
      url: 'https://www.aex.com/'
    },
    {
      name: 'Cryptopia',
      url: 'https://www.cryptopia.co.nz/Exchange/?market=CAN_BTC'
    },
    {
      name: 'CoinSpot',
      url: 'https://www.coinspot.com.au/buy/can'
    },
    {
      name: 'EtherDelta',
      url: 'https://etherdelta.com/#0x1d462414fe14cf489c7a21cac78509f4bf8cd7c0-ETH'
    },
    {
      name: 'Coss.io',
      url: 'https://exchange.coss.io/exchange/can-eth'
    },
    {
      name: 'Qryptos',
      url: 'https://trade.qryptos.com/basic/CANETH'
    },
    {
      name: 'Radar Relay',
      url: 'https://app.radarrelay.com/CAN/WETH'
    },
    {
      name: 'Gatecoin',
      url: 'https://gatecoin.com/markets/caneth'
    },
    {
      name: 'Coinswitch',
      url: 'https://coinswitch.co/'
    },
    {
      name: 'IDAX',
      url: 'https://www.idax.mn/#/exchange?pairname=CAN_ETH'
    }
  ];
  teamMembers = [
    {
      name: 'John-Paul Thorbjornsen',
      position: 'Co-Founder',
      linkedin: 'https://www.linkedin.com/in/jp-thor/',
      img: 'assets/img/people/team/jp.jpg'
    },
    {
      name: 'Chris McLoughlin',
      position: 'Co-Founder',
      linkedin: 'https://www.linkedin.com/in/christopher-mcloughlin-757a56128/',
      img: 'assets/img/people/team/chris.jpg'
    },
    {
      name: 'Kyle Hornberg',
      position: 'Co-Founder',
      linkedin: 'https://www.linkedin.com/in/kyle-hornberg/',
      img: 'assets/img/people/team/kyle.jpg'
    },
    {
      name: 'Rowan Wilson',
      position: 'Co-Founder',
      linkedin: 'https://www.linkedin.com/in/rowan-willson/',
      img: 'assets/img/people/team/rowan.jpg'
    },
    {
      name: 'Cameron Stuart',
      position: 'Developer',
      linkedin: 'https://www.linkedin.com/in/cameron-stuart-b274739/',
      img: 'assets/img/people/team/cam.jpg'
    },
    {
      name: 'Alex Scott',
      position: 'Developer',
      linkedin: 'https://www.linkedin.com/in/alex-scott-0341a976/',
      img: 'assets/img/people/team/alex.png'
    },
    {
      name: 'Wie Cahya',
      position: 'Developer',
      linkedin: 'https://www.linkedin.com/in/wie-cahya/',
      img: 'assets/img/people/team/wie.jpg'
    },
    {
      name: 'Johan Deecke',
      position: 'Community Manager',
      linkedin: 'https://www.linkedin.com/in/johan-deecke-371111163/',
      img: 'assets/img/people/team/johan.png'
    },
    {
      name: 'Steve Meacham',
      linkedin: 'https://www.linkedin.com/in/stevenmeacham/',
      position: 'Digital Marketing',
      img: 'assets/img/people/team/steven.jpg'
    }
  ];
  advisors = [
    {
      name: 'Sebastian Quinn Watson',
      desc: 'CanYa, Power Ledger, Bluzelle',
      linkedin: 'https://www.linkedin.com/in/sebastian-qw-645457b3/',
      img: 'assets/img/people/advisors/sebastian.jpg'
    },
    {
      name: 'Rob Morris',
      desc: 'CEO and Founder, Prismatik',
      linkedin: 'https://www.linkedin.com/in/robmorris/',
      img: 'assets/img/people/advisors/rob.jpg'
    },
    {
      name: 'Josh Buirski',
      desc: 'Distributed Technologies Institute',
      linkedin: 'https://www.linkedin.com/in/joshuambuirski/',
      img: 'assets/img/people/advisors/joshua.jpg'
    },
    {
      name: 'Kai C. Chng',
      desc: 'Co-Founder & CEO of DigixGlobal',
      linkedin: 'https://www.linkedin.com/in/kai-c-chng-540b339/',
      img: 'assets/img/people/advisors/kai.jpg'
    },
    {
      name: 'James Waugh',
      desc: 'Founder of Blueblock.io',
      linkedin: 'https://www.linkedin.com/in/blueblock/',
      img: 'assets/img/people/advisors/james.jpg'
    },
    {
      name: 'Layla Tabatabaie',
      desc: 'Attorney, Author and Advisor',
      linkedin: 'https://www.linkedin.com/in/layla-tabatabaie-94089883/',
      img: 'assets/img/people/advisors/layla.jpg'
    },
    {
      name: 'Taotao He',
      desc: 'Co-Founder at Titan Digital Asset Group',
      linkedin: 'https://www.linkedin.com/in/taotao-he-116b7948/',
      img: 'assets/img/people/advisors/taotao.jpg'
    },
    {
      name: 'Thomas Graham',
      desc: 'TLDR',
      linkedin: 'https://www.linkedin.com/in/tomgtgraham/',
      img: 'assets/img/people/advisors/thomas.jpg'
    }
  ];
  partners = [
    {
      name: 'Digix',
      desc: 'Integration of the Digix gold-backed token (DGX).',
      img: 'assets/img/partners/digix.png',
      url: 'https://digix.global/'
    },
    {
      name: 'Indorse',
      desc: 'Integration of crowd-sourced skill verifications.',
      img: 'assets/img/partners/indorse.svg',
      url: 'https://indorse.io/'
    },
    {
      name: 'Madana',
      desc: 'Integration of market for data analysis.',
      img: 'assets/img/partners/madana.jpg',
      url: 'https://www.madana.io/'
    },
    {
      name: 'Trust Wallet',
      desc: 'Trust - fast and secure Ethereum wallet that’s easy to use.',
      img: 'assets/img/partners/trust.jpg',
      url: 'https://trustwalletapp.com//'
    },
    {
      name: 'qiibee',
      desc: 'Integration of a decentralized, blockchain-based loyalty ecosystem',
      img: 'assets/img/partners/qiibee.png',
      url: 'https://qiibee.com/'
    },
    {
      name: 'Giveth',
      desc: 'Integration of charitable giving on the blockchain.',
      img: 'assets/img/partners/giveth.png',
      url: 'https://giveth.io/'
    },
    {
      name: 'Gladius',
      desc: 'Integration of a decentralized solution to protect against DDoS attacks.',
      img: 'assets/img/partners/gladius.png',
      url: 'https://gladius.io/'
    },
    {
      name: 'Origin',
      desc: 'The sharing economy without intermediaries',
      img: 'assets/img/partners/origin.png',
      url: 'https://www.originprotocol.com/'
    },
    {
      name: 'Blueblock',
      desc: 'Building the next generation of token economy',
      img: 'assets/img/partners/blueblock.png',
      url: 'https://www.blueblock.io/'
    }

  ];
  // Flags
  loading = true;
  isOnMobile = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    console.log('Welcome to CanYa, The decentralised serviceplace for the world !');
    const ua = window.navigator.userAgent;
    this.isOnMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua);

  }

  ngAfterViewInit() {
    const blurbs = [
      'The CanYa Ecosystem',
      'Universal payment system',
      'CanYa’s Payment escrow',
      'Decentralised platform',
      '1% service fee',
      'Up to 20x cheaper to use than other platforms',
      'Smart Contract Verifiability',
      'Handpicked talent',
      'Pay by crypto',
      'Over 13000 active Telegram chat members',
      'Over 77000 Facebook page followers',
      'Over 19000 Twitter followers',
      'Over 2500 ICO contributors',
      'Partnerships with leading crypto projects',
      'P2P marketplace of skilled services',
      'Partnerships with leading crypto projects'
    ];
    const typed = new Typed('#why-canya', {
      strings: blurbs,
      typeSpeed: 50,
      backSpeed: 15,
      backDelay: 1000,
      showCursor: false,
      loop: true,
      loopCount: Infinity
    });
    const latestNews = [];

  }
}
