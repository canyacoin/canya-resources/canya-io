import { Component, OnInit } from '@angular/core';
declare var createFooter: any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  canAppsFooter = [
    {
        name            :   'CanWork',
        desc            :   'The decentralised marketplace of services',
        url             :   'https://Canwork.io',
        type            :   'main'
    },
    {
        name            :   'CanInvoice',
        desc            :   'Easily create your own clean, professional and accurate invoices',
        url             :   'https://caninvoice.io',
        type            :   'helper'
    },
    {
        name            :   'CanStation',
        desc            :   'Find out the ideal amount of gas to allocate to your ETH transaction',
        url             :   'https://CanStation.io',
        type            :   'helper'
    },
    {
        name            :   'CanSend',
        desc            :   'Send ERC20 tokens to multiple addresses at once',
        url             :   'https://CanSend.io',
        type            :   'helper'
    },
    {
        name            :   'CanShare',
        desc            :   'Send and receive files using distributed technology',
        url             :   'https://canshare.io/',
        type            :   'helper'
    },
    {
        name            :   'CanTrack',
        desc            :   'A crisp and easy to use task tracker to help you with your project',
        url             :   'https://CanTrack.io',
        type            :   'helper'
    },
    {
        name            :   'CanSign',
        desc            :   'Upload documents to IPFS and sign them with your Ethereum address.',
        url             :   'https://CanSign.io',
        type            :   'helper'
    },
    {
        name            :   'CanSeek',
        desc            :   'Hire and be hired with decentralised technology',
        url             :   'https://CanSeek.io',
        type            :   'main'
    }
];
  community = [
    { name : 'Facebook', url : 'https://www.facebook.com/CanYaCoin/', className : 'social-facebook', fontAwesome : 'fab fa-facebook-square' },
    { name : 'Twitter', url : 'https://twitter.com/canyacoin' , className : 'social-twitter', fontAwesome : 'fab fa-twitter'},
    { name : 'Instagram', url : 'https://www.instagram.com/canyacoin/', className : 'social-instagram', fontAwesome : 'fab fa-instagram' },
    { name : 'Youtube', url : 'https://www.youtube.com/channel/UCbbpsDWjpdC-4LjDnF4Ec6w/',  className : 'social-youtube', fontAwesome : 'fab fa-youtube' },
    { name : 'Github', url : 'https://github.com/canyaio/', className : 'social-github', fontAwesome : 'fab fa-github'},
    { name : 'Telegram', url : 'https://t.me/joinchat/GI97FhDD1lf6dh-r9XRdvA', className : 'social-telegram', fontAwesome : 'fab fa-telegram'}
  ];

  constructor() { }

  ngOnInit() {

  }

}
