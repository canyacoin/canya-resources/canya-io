
//Creates header for bootstrap /CanYa angular apps.
function createHeader(headerImgUrl, urls, primaryBtn, secondaryBtn){ 
	
	var nav_container = document.createElement("div");
	nav_container.className = "container";
	
	var topbar_left = document.createElement("div");
	topbar_left.className = "topbar-left";
	var toggler = document.createElement("button");
	toggler.className = "topbar-toggler text-gray";
	var toggler_i = document.createElement("i");
	toggler_i.className = "fas fa-bars";
	toggler.appendChild(toggler_i);
	
	topbar_left.appendChild(toggler);
	
	var logo_a = document.createElement("a");
	var logo_img = document.createElement("img");
	
	logo_a.className = "topbar-brand";
	
	logo_img.src = headerImgUrl;
	logo_img.className = "topbar-brand";
	logo_img.style.height = "36px";
	logo_img.style.width = "auto";

	
	logo_a.appendChild(logo_img);
	
	topbar_left.appendChild(logo_a);
	var topbar_right = document.createElement("div");
	topbar_right.className = "topbar-right";
	
	topbar_ul = document.createElement("ul");
	topbar_ul.id = "top-nav"; 
	topbar_ul.className = "topbar-nav nav mr-3";
	
	if(urls.length > 0){
	for (var i = 0; i < urls.length; i++){
		var li = document.createElement("li");
		li.className = "nav-item";
		var a = document.createElement("a");
		a.innerHTML = urls[i].text;
		a.href = urls[i].href;
		a.className = "nav-link";
		a.target = "_blank";
		li.appendChild(a);
		topbar_ul.appendChild(li);
	   }
	}
    
	//create the buttons
	var primary_btn = document.createElement("button");
	primary_btn.className = "btn btn-primary btn-sm";
	primary_btn.innerHTML = primaryBtn.text;
	var primary_btn_a = document.createElement("a");
	primary_btn_a.href = primaryBtn.href;
	primary_btn_a.target = "_blank"; 
	primary_btn_a.appendChild(primary_btn);
	
	//create the secondary buttons 
    var secondary_btn = document.createElement("button");
	secondary_btn.className = "btn btn-primary btn-outline btn-sm mx-1";
	secondary_btn.innerHTML = secondaryBtn.text;
	var secondary_btn_a = document.createElement("a");
	secondary_btn_a.href = secondaryBtn.href;
	secondary_btn_a.target = "_blank"; 
	secondary_btn_a.appendChild(secondary_btn);
    console.log(secondary_btn_a);
    
    
	topbar_right.appendChild(topbar_ul);
    topbar_right.appendChild(secondary_btn_a);
	topbar_right.appendChild(primary_btn_a);
    
	nav_container.appendChild(topbar_left);
	nav_container.appendChild(topbar_right);
	
	
	document.getElementById("section-top").appendChild(nav_container);

	
}