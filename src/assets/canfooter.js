/*
	WARNING! MAKE SURE YOU ALREADY HAVE, AND IS USING, CANYA.CSS AND HAVE ALREADY EMBEDDED FONT-AWESOME!
*/
//footer navs
var navUrls = [
    { name : "Home", url : "https://canya.io/" },
    { name : "Blog", url : "https://blog.canya.com.au/" },
    { name : "Forum", url : "https://forum.canya.io/"}
    
];
//social icon navs
var socialIcons = [ 
    { name : "facebook", url : "https://www.facebook.com/CanYaCoin/", className : "social-facebook", fontAwesome : "fab fa-facebook-square" },
    { name : "twitter", url : "https://twitter.com/canyacoin" , className : "social-twitter", fontAwesome : "fab fa-twitter"},
    { name : "instagram", url : "https://www.instagram.com/canyacoin/", className : "social-instagram", fontAwesome : "fab fa-instagram" },
    { name : "youtube", url : "https://www.youtube.com/channel/UCbbpsDWjpdC-4LjDnF4Ec6w/",  className : "social-youtube", fontAwesome : "fab fa-youtube" },
    { name : "github", url : "https://github.com/canyaio/", className : "social-github", fontAwesome : "fab fa-github"}, 
    { name : "telegram", url : "https://t.me/joinchat/GI97FhDD1lf6dh-r9XRdvA", className : "social-telegram", fontAwesome : "fab fa-telegram"} 
];
var canApps = [
    {
        name            :   'CanInvoice',
        logo            :   'https://canya.io/images/CanApps/logos/CanInvoice.svg',
        url             :   'https://caninvoice.io'
    },
    {
        name            :   'CanStation',
        logo            :   'https://canya.io/images/CanApps/logos/CanStation.svg',
        url             :   'https://CanStation.io'
    },
    {
        name            :   'CanFund',
        logo            :   'https://canya.io/images/CanApps/logos/CanFund.svg',
        url             :   'https://CanFund.io'
    },
    {
        name            :   'CanSend',
        logo            :   'https://canya.io/images/CanApps/logos/CanSend.svg',
        url             :   'https://CanSend.io'
    },
    {
        name            :   'CanShare',
        logo            :   'https://canya.io/images/CanApps/logos/CanShare.svg',
        url             :   'https://canshare.io/'
    },
    {
        name            :   'CanTrack',
        logo            :   'https://canya.io/images/CanApps/logos/CanTrack.svg',
        url             :   'https://CanTrack.io'
    },
    {
        name            :   'CanSign',
        logo            :   'https://canya.io/images/CanApps/logos/CanSign.svg',
        url             :   'https://CanSign.io'
    },
    {
        name            :   'CanSeek',
        logo            :   'https://canya.io/images/CanApps/logos/CanSeek.svg',
        url             :   'https://CanSeek.io'
    },
    {
        name            :   'CanWork',
        logo            :   'https://canya.io/images/CanApps/logos/CanWork.svg',
        url             :   'https://CanSeek.io'
    }
]; 


function createCustomFooter(customLinks){ 
    var footer_container = createDiv("container");
    var footer_row = createDiv("row gap-y align-items-center"); 
    //first left div
    var footer_img_div = createDiv("col-12 col-lg-3");
    var footer_img_p = createElement("p","text-center text-lg-left");
    var footer_img_a = createLink("https://canya.io");
    var footer_img = createImg("https://canya.io/images/CanYa_logo.png","auto","25px");
    footer_img_a.appendChild(footer_img);
    footer_img_p.appendChild(footer_img_a);
    footer_img_div.appendChild(footer_img_p);
    //second middle div
    var footer_nav_div = createDiv("col-12 col-lg-5");
    var footer_nav_ul = createElement("ul","nav nav-primary nav-hero");
    footer_nav_div.appendChild(footer_nav_ul);
    for(var i = 0 ; i < navUrls.length; i++){
        var li = createElement("li","nav-item");
        var a = createLink(navUrls[i].url,navUrls[i].name,"nav-link");
        li.appendChild(a);
        footer_nav_ul.appendChild(li);
    }
    var customURLs = customLinks;
    console.log(customURLs != undefined);
    if(customURLs != undefined){
            console.log("creating links for "+customURLs);
        for(var i=0; i<customURLs.length ;i++){ 
            console.log("creating links for "+customURLs[i].name);
            var li = createElement("li","nav-item");
            var a = createLink(customURLs[i].url,customURLs[i].name,"nav-link");
            li.appendChild(a);
            footer_nav_ul.appendChild(li);
        }
    } 
    //last, social icon divs
    var footer_soc_container = createDiv("social social-lg text-center text-lg-right");
    var footer_soc_div = createDiv("col-12 col-lg-4");  
    footer_soc_div.appendChild(footer_soc_container);
	 for (var i = 0 ; i <socialIcons.length; i++){ 
        var a = createLink(socialIcons[i].url,"",socialIcons[i].className); 
        var fab = createFAIcon(socialIcons[i].fontAwesome); 
		a.appendChild(fab); 
		footer_soc_container.appendChild(a);
    }
    footer_row.appendChild(footer_img_div); 
    footer_row.appendChild(footer_nav_div);
    footer_row.appendChild(footer_soc_div);
    footer_container.appendChild(footer_row);
    
    document.getElementById('canYaFooter').appendChild(footer_container);
}  
function createFooter(){ 
    //footer container
    var footer_container = document.createElement("div");
    footer_container.className = "container";
    //footer row
    var footer_row = document.createElement("div");
    footer_row.className= "row gap-y align-items-center";
    footer_container.appendChild(footer_row);
    //first left div
    var footer_img_div = document.createElement("div");
    footer_img_div.className = "col-12 col-lg-3";
    var footer_img_p = document.createElement("p");
    footer_img_p.className="text-center text-lg-left";
    var footer_img_a = document.createElement("a");
    footer_img_a.href = 'https://canya.io';
    footer_img_a.target = "_blank";
    var footer_img_img = document.createElement("img");
    footer_img_img.style = "height:25px; width: auto"
    footer_img_img.src = "https://canya.io/images/CanYa_logo.png"; 
    footer_img_a.appendChild(footer_img_img);
    footer_img_p.appendChild(footer_img_a);
    footer_img_div.appendChild(footer_img_p);
    footer_row.appendChild(footer_img_div);
    var footer_nav_div = document.createElement("div");
    //second middle div
    footer_nav_div.className = "col-12 col-lg-5";
    footer_row.appendChild(footer_nav_div);
    var footer_nav_ul = document.createElement("ul");
    footer_nav_ul.className = "nav nav-primary nav-hero";
    footer_nav_div.appendChild(footer_nav_ul); 
    for(var i = 0 ; i < navUrls.length; i++){
        var li = document.createElement("li");
        li.className = "nav-item";
        var a = document.createElement("a");
        a.innerHTML =  navUrls[i].name;
        a.href =  navUrls[i].url;
        a.className = "nav-link"
		a.target = "_blank";
        li.appendChild(a);
        footer_nav_ul.appendChild(li);
    }
    //third and last col.
    var footer_soc_div = document.createElement("div");
    var footer_soc_container = document.createElement("div");
    footer_soc_container.className = "social social-lg text-center text-lg-right";
    footer_soc_div.className = "col-12 col-lg-4";  
    //create the social icons
	 for (var i = 0 ; i <socialIcons.length; i++){
        var a = document.createElement("a");
        a.className = socialIcons[i].className;
		a.href = socialIcons[i].url;
		var fab = document.createElement("i"); 
		fab.className = socialIcons[i].fontAwesome;
		a.appendChild(fab);
        a.target = "_blank";
		footer_soc_container.appendChild(a);
    } 
    footer_soc_div.appendChild(footer_soc_container);
    footer_row.appendChild(footer_soc_div); 
	//create the footer.
    document.getElementById('canYaFooter').appendChild(footer_container);
}
function createFAIcon(classes){
    var fa = document.createElement("i");
    fa.className=classes;
    return fa;
}
function createElement(type, classes){
    var element = document.createElement(type);
    element.className = classes;
    return element;
}
function createDiv(classNames){
    var div = document.createElement("div");
    div.className = classNames;
    return div;
}
function createImg(imgLink,width,height){
    var img =  document.createElement("img");
    img.src = imgLink;
    img.style = "width:"+width+";height:"+height+";";
    return img;
}
function createLink(url,inner,classes){
    var link = document.createElement("a");
    if(inner != undefined){
        link.innerHTML = inner;
    }
    if(classes != undefined){
        link.className = classes;
    }
    link.target = "_blank";
    link.href = url;
    return link;
} 